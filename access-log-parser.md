# access-log-parser
Access log parser for nginx / apache

## This script will output an array with:
- suspicious requests
- malicious requests
- total requests
- success requests
- failed requests
- top files percent
- top referers percent
- top uas percent
