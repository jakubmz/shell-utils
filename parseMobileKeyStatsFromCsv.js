/* Registration No.
Unique registration number
Status
User ID
Last name
First name
User group
Card name
Issue time
Start time
Expiration time
Cancel time
Main guest room
Issued by
Cancelled by
Time schedule
Saved
S/N
Device name
Endpoint
Account name

*/

/* CSV EXAMPLE
[
  '102701',
  '1002700',
  'Overwritten',
  'ID20220323173131',
  '',
  '',
  'Guest',
  'Joiner (MC)',
  '4/2/2022 8:27 AM',
  '4/2/2022 8:27 AM',
  '2/1/2023 11:00 PM',
  '4/6/2022 1:48 PM',
  '946',
  'dle2',
  'ksanjose',
  '',
  '',
  'ffffffffffffffff',
  'Mobile Access',
  '5a1e6ef3-9e20-4870-8941-0029cda8ef9a',
  'Riseio\r'
]


*/

/*
 * Extract Assa Abbloy mobile key stats from Visionline exported .xls => exported to .csv
 *
 * Author: Jakub Michal Zawada Zegan (jakubmz@gmail.com)
 */

const fs = require('fs');

const file = process.argv[2];

if (!file) {
    console.error('Use: node parseMobileKeyStatsFromCsv.js mobile-keys.csv');
    return;
}

fs.readFile(file, 'utf8', (err, data) => {
    'use strict';
    if (err) {
        return console.error(err);
    }
    console.log(data);

    const keysArray = data.split('\n');

    const stats = {
        units: {},
        unitsWithoutValidKeys: [],
        canceledKeyDurationsHours: [],
        totalUnits: 0,
        totalKeys: 0,
        totalCanceledKeys: 0,
        totalCanceledKeysLess30days: 0,
        totalCanceledKeysLess7days: 0,
        totalCanceledKeysLess1day: 0,
        totalCanceledKeysLess1hour: 0,
        totalExpiredKeys: 0,
        totalValidKeys: 0,
        totalNotIssuedKeys: 0,
        avgCanceledKeyDurationHours: 0,
    };

    keysArray.forEach(line => {

        let keyArray = line.split(';');

        let deviceName = keyArray[18];

        // Skip normal cardss
        if (deviceName !== 'Mobile Access') {
            return;
        }

        // console.log(line);
        // console.log(keyArray);

        let uniqueId = keyArray[1];
        let status = keyArray[2];
        let unitNumber = keyArray[12];
        let issueDate = keyArray[9];
        let expirationDate = keyArray[10];
        let cancelDate = keyArray[11];

        // console.debug(uniqueId, status, unitNumber, issueDate, expirationDate, cancelDate);

        stats.totalKeys++;

        if (!stats.units[unitNumber]) {
            stats.units[unitNumber] = [];
            stats.totalUnits++;
        }
        stats.units[unitNumber].push(keyArray);

        // if (status === 'Overwritten' || cancelDate) {
        if (cancelDate) {
            stats.totalCanceledKeys++;
            let keyDurationHours = Number(new Date(cancelDate).getTime() - new Date(issueDate).getTime()) / 1000 / 60 / 60;
            stats.canceledKeyDurationsHours.push(keyDurationHours);
        }
        if (new Date(expirationDate).getTime() <= new Date().getTime()) {
            stats.totalExpiredKeys++;
        }
        if (status === 'Valid') {
            stats.totalValidKeys++;
        }
        if (status === 'Not issued') {
            stats.totalNotIssuedKeys++;
        }
    });

    // Get units without valid keys
    const hasUnitValidKey = unit => !!unit.find(k => k[2] === 'Valid');
    const getUnitNumberFromFirstKey = unit => unit[0][12];
    stats.unitsWithoutValidKeys = Object.values(stats.units)
        .filter(unit => !hasUnitValidKey(unit))
        .map(unit => getUnitNumberFromFirstKey(unit));

    // Get canceled detailed stats
    const getTotalCanceledKeysInLessThan = (durationsArr, lessThanHours) =>
        durationsArr.reduce((acc, cur) => (cur < lessThanHours) ? acc += 1 : acc, 0);
    stats.totalCanceledKeysLess30days = getTotalCanceledKeysInLessThan(stats.canceledKeyDurationsHours, 30 * 24);
    stats.totalCanceledKeysLess7days = getTotalCanceledKeysInLessThan(stats.canceledKeyDurationsHours, 7 * 24);
    stats.totalCanceledKeysLess1day = getTotalCanceledKeysInLessThan(stats.canceledKeyDurationsHours, 1 * 24);
    stats.totalCanceledKeysLess1hour = getTotalCanceledKeysInLessThan(stats.canceledKeyDurationsHours, 1);

    // Get avg key duration
    const arraySum = arr => arr.reduce((accuVariable, curValue) => accuVariable + curValue, 0);
    stats.avgCanceledKeyDurationHours = Math.round(arraySum(stats.canceledKeyDurationsHours) / stats.canceledKeyDurationsHours.length);
    stats.avgCanceledKeyDurationDays = Math.round(stats.avgCanceledKeyDurationHours / 24);

    console.log(stats);

});
