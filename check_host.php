<?php /* CHECK HOST PING RESPONSE TIME & ALERT */

date_default_timezone_set('Europe/Madrid');

$host = !empty($argv[1]) ? $argv[1] : 'yoursite.com';
$email = 'your@email.com';
$email_sent = false;
$error_detected = 0;
$log = "\n---".date('Y-m-d')."---";
echo "\n---".date('Y-m-d')."---";

for($i=0; $i<55; $i++) {

  $o = trim(shell_exec("ping -c 1 -W 1000 $host"));
  $line = explode("\n", $o);
  $l1 = isset($line[1])? $line[1] : $o;
  $l2 = isset($line[4]) ? $line[4] : $o;
  $time = preg_replace("/^.*time=/",'', $l1);
  $time = preg_replace("/[^\d\.]/",'', $time);

  $error = '';
  if(!preg_match("/0% packet loss/i", $l2)) {
    $error .= "PING LOSS [$host]";
    $error_detected++;
  }
  if(!preg_match("/^\d{1,} bytes from.*\d{1,}\.\d{1,}\.\d{1,}\.\d{1,}.* icmp_seq=\d{1,} ttl=\d{1,} time=\d{1,}.*ms$/i", $l1)) {
    $error .= "PING ERROR [$host]";
    $error_detected++;
  }
  if($time > 999) {
    $error .= "PING TOO SLOW [$host]";
    $error_detected++;
  }

  echo "\n".date('H:i:s')." {$time}ms";
  $log .= "\n".date('H:i:s')." {$time}ms";
  sleep(1);
}


// * SEND FINAL EMAIL
if($error_detected >= 3) {
  $email_subject = "CHECK HOST ALERT ";
  $message = "<h1>$email_subject</h1><h4>$error</h4><pre>$log</pre>";
  $headers = "From: $email\r\nCC: $email\r\nMIME-Version: 1.0\r\nContent-Type: text/html; charset=ISO-8859-1\r\n";
  mail($email, $email_subject, $message, $headers);
}
?>
