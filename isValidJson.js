/*
 * Validates JSON file
 */
'esversion: 6';

const fs = require('fs');

const scriptName = process.argv[1].replace(/^.*\//, '');
const file = process.argv[2];

if (!file) {
    console.error(`Use: node ${scriptName} file.json`);
    return;
}

fs.readFile(file, 'utf8', (err, data) => {
    'use strict';
    if (err) {
        return console.error(err);
    }

    try {
        JSON.parse(data);
    } catch (e) {
        console.error('❌ Invalid JSON detected! 🙁', file);
        console.error(e);
        return false;
    }
    console.info('✅ JSON validated successfully! 😊', file);
    return true;


});