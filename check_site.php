<?php /* CHECK SITE LOADING TIME & ALERT */

date_default_timezone_set('Europe/Madrid');

$site = !empty($argv[1]) ? $argv[1] : 'https://your-website.com';
$email = 'your@email.com';
$email_sent = false;
$email_subject = "CHECK SITE ALERT ";
$error_detected = false;
$critical_error_detected = 0;


function sendEmail($subject='', $message='', $email='')
{
  require_once "Mail.php";
  require_once "Mail/mime.php";

  //$smtp = Mail::factory('mail');
  $smtp = Mail::factory('smtp', array('host' => 'ssl://smtp.gmail.com',
                                      'port' => 465,
                                      'auth' => true,
                                      'username' => 'your@email.com',
                                      'password' => 'your-password'));
  $mime_params = array('text_encoding' => '7bit', 'text_charset'  => 'UTF-8', 'html_charset'  => 'UTF-8', 'head_charset'  => 'UTF-8');
  $mime = new Mail_mime();
  $mime->setHTMLBody($message);
  $body = $mime->get($mime_params);
  $headers = $mime->headers(array('From'=>$email, 'To'=>$email, 'Subject'=>$subject, 'Content-Type'=>'text/html; charset=UTF-8'));
  $mail = $smtp->send($email, $headers, $body);
}

$log = "\n---".date('Y-m-d')."---";
echo "\n---".date('Y-m-d')."---";
$t0 = date('U');
while(true) {
  $t1 = date('U');
  if($t1 - $t0 >= 55) break;

  $o = strip_tags(trim(file_get_contents($site)));
  $t2 = date('U');
  $o = preg_replace("/\s{1,}/", ' ', $o);
  $load_time = ($t2-$t1)."sec";
  //echo "\n\n$load_time:\n$o\n\n\n";

  $error = '';
  if(empty($o)) {
    $error .= "BLANK SCREEN [$site]";
    $critical_error_detected++;
  }
  if($load_time >= 30) {
    $error .= "CRITICAL SLOW $load_time [$site]";
    $critical_error_detected++;
  } elseif($load_time >= 15) {
    $error .= "VERY SLOW $load_time [$site]";
    $critical_error_detected++;
  } elseif($load_time > 7 AND $load_time < 15) {
    $error .= "SLOW $load_time [$site]";
  }


  echo "\n".date('H:i:s')." $load_time ".($error ? $error : "ok");
  $log .= "\n".date('H:i:s')." $load_time ".($error ? $error : 'ok');
  // * SEND FIRST EMAIL
  if($critical_error_detected AND !$email_sent) {
    $message = "<h1>$email_subject</h1><h2 style='color:red'>$error</h2>";
    sendEmail($email_subject, $message, $email);
    $email_sent = true;
    $error_detected = true;
  }
  sleep(5);
}

// * SEND FINAL EMAIL
if($error_detected) {
  $message = "<h1>$email_subject</h1><pre style='color:red; font-size:18px;'>$log</pre>";
  sendEmail($email_subject, $message, $email);
}
?>
