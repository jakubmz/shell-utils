#!/bin/sh
# Wordpress deploy script

USER=kuba
HOST=mufrons2
REMOTE_PATH=/home/kuba/projects/tenerifelodge

CODE=yes
DB=yes
FILES=yes

if [ "$1" == "code" ] ; then
    echo "PARTIAL DEPLOY: only code."
    DB=no
    FILES=no
elif [ "$1" == "db" ] ; then
    echo "PARTIAL DEPLOY: only database."
    CODE=no
    FILES=no
elif [ "$1" == "files" ] ; then
    echo "PARTIAL DEPLOY: only files."
    CODE=no
    DB=no
else
    echo "FULL DEPLOY: code, database and files."
fi


echo "--- RUNNING DEPLOY ..."
date


if [ "$DB" == "yes" ] ; then
    echo
    echo "--- LOCAL DB EXPORT AND GIT PUSH ..."
    docker exec -it tenerifelodge_db_1 mysqldump -u wordpress wordpress -pwordpress |grep -v "mysqldump: .Warning" |grep -v "mysqldump: Error" > ./db-dump/tenerifelodge-db.sql
    git fetch
    git pull
    git add ./db-dump/tenerifelodge-db.sql
    git commit -m 'DB updated.'
    git push -u origin master
fi

# if [ "$CODE" == "yes" ] ; then
#     echo "--- LOCAL GIT COMMIT AND PUSH ..."
#     git add stockholm-child
#     git commit -m 'Code updated.'
#     git push
# fi

if [ "$CODE" == "yes" ] || [ "$DB" == "yes" ] ; then
    echo
    echo "--- REMOTE GIT SYNC ..."
    ssh ${USER}@${HOST} "cd ${REMOTE_PATH}; git fetch; git pull;"
fi

if [ "$DB" == "yes" ] ; then
    echo
    echo "--- REMOTE DB SYNC ..."
    DUMP_FILE=tenerifelodge-db.sql
    ssh ${USER}@${HOST} "docker exec tenerifelodge_db_1 bash -c \" mysql -uwordpress -pwordpress wordpress < /tmp/db-dump/${DUMP_FILE}\""
fi


if [ "$FILES" == "yes" ] ; then
    echo
    echo "--- REMOTE FILE SYNC ..."
    rsync -zarvh --no-perms --include ".*" uploads/ $USER@$HOST:$REMOTE_PATH/uploads --delete
    rsync -zarvh --no-perms --include ".*" plugins/ $USER@$HOST:$REMOTE_PATH/plugins --delete
fi

echo
date
echo "--- DEPLOY COMPLETE!"
