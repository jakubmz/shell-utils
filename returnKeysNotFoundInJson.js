/*
 * Filter keys that already exists in 2 json file
 *
 * JSON file must contain an object like:
 * {
 *   key: value,
 *   key2: value2,
 *   keyN: valueN
 * }
 *
 * keys file must contain plain text list like:
 *   key
 *   key2
 *   keyN
 *
 * @author jakubmz@gmail.com
 */
'esversion: 6';

const fs = require('fs');

const scriptName = process.argv[1].replace(/^.*\//, '');
const jsonFile = process.argv[2];
const keysFile = process.argv[3];

if (!jsonFile || !keysFile) {
    console.error(`Use: node ${scriptName} file.json keys.txt`);
    return;
}

fs.readFile(jsonFile, 'utf8', (err, data) => {
    'use strict';
    if (err) {
        return console.error(err);
    }

    let jsonObj = {};
    try {
        jsonObj = JSON.parse(data);
        // console.debug(jsonObj);
    } catch (err) {
        return console.error(err);
    }

    fs.readFile(keysFile, 'utf8', (err2, data2) => {
        if (err2) {
            return console.error(err2);
        }
        data2.split('\n').forEach(l => (jsonObj[l] ? false : console.log(l)));
    });
});