/*
 * Optimize Images in current folder Script (batch)
 *
 * Optimizes all images inside a local folder
 *
 * ACTIONS:
 * - check size
 * - upscale ( DeepAI waifu2x) if image width detected is less than 1000
 * - convert to jpg,
 * - resize,
 * - set quality
 * - normalize file names
 *
 * USE:
 * 1. Copy this script inside folder with images
 * 2. Instal deps: yarn install
 * 2: Run:
 * node optimize-images.js height=900 quality=75 prefix=text ext=png padNumbers upscaleDeepAiKey=hash
 */
'esversion: 7';

const path = require('path');
const fs = require('fs');
const Jimp = require('jimp');
const directoryPath = path.join(__dirname, '.');
const deepai = require('deepai');

// Get param helper
const getArgByKey = key => {
    const keyFound = process.argv.find(a => a.includes(`${key}`));
    if (keyFound) {
        return keyFound.includes('=') ? keyFound.replace(`${key}=`, '') : true;
    }
    return false;
}

// CONFIG PARAMS
const params = {
  height: parseInt(getArgByKey('height')) || 800, // result height
  quality: parseInt(getArgByKey('quality')) || 70, // result quality
  prefix:  getArgByKey('prefix') || '', // append prefix
  ext: getArgByKey('ext') || 'png', // result ext
  upscaleDeepAiKey: getArgByKey('upscaleDeepaiKey') || false, // 2x Waifu image resolution b
  padNumbers: getArgByKey('padNumbers') || false // FIX file sorting
};

console.log(process.argv);
console.log('\n\noptimize-images.js running with params', params);

if (params.upscaleDeepAiKey) {
    console.log(`\nEnabling upscaling with DeepAi API Key ${params.upscaleDeepAiKey} ... `);
    deepai.setApiKey(params.upscaleDeepAiKey);
}

const getFileExt = filename => String(filename).replace(/^.*\./g, '.');

const removeFileExt = filename => String(filename).replace(/\.\w{3,4}$/g, '');

const getNumber = filename => String(filename).replace(/[^\d]/g, '');

const addPrefix = prefix => (prefix ? prefix + '-' : '');

const prettify = str => String(str).toLowerCase().replace(/ /g, '-').replace(/--/g, '-').replace(/--/g, '-');

const padNumbers = str => {
    const num = getNumber(str);
    if (!String(num).length) {
      return str;
    }
    const letters = str.replace(/\d/g, '');
    return (letters ? letters + '-' : '') + num.padStart(6, '0');
};

const getImageSize = async file => {
    return new Promise(resolve => {
        new Jimp(file, (err, image) => resolve({ width: image.bitmap.width, height: image.bitmap.height }));
    });
};

const upscaleImage = async fileToUpscale => {
    console.log('#2 Upscaling image (DeepAI Waifu2x API) 🔧 <==', fileToUpscale);
    return await (async function () {

        const resp = await deepai.callStandardApi('waifu2x', {
            image: fs.createReadStream(fileToUpscale)
        });
        console.log('🚀 DeepAI Waifu2x API response', resp);
        return resp.output_url;
    })();
};

const normalizeImageFile = (file, finalFile, height, quality) => {
    console.debug('#3 Normalizing image file to height', file, finalFile, height);
    Jimp.read(file)
        .then(f => {
            return f
                .resize(Jimp.AUTO, height) // resize
                .quality(quality) // set JPEG quality
                .writeAsync(finalFile, Jimp.AUTO); // save
        })
        .then(() => console.log('READY => ', finalFile))
        .catch(err => console.error(err));
};

// const downloadFileToDisk = async (url, localPath) => {
//     const file = fs.createWriteStream(localPath);
//     const request = await https.get(url, response => response.pipe(file));
// };

// const deleteFile = upscaledFile => {
//     console.log('#4 Deletting temp upscaled file:', upscaledFile);
//     fs.unlinkSync(upscaledFile);
// };

const copyFile = (srcFilePath, destFilePath) => {
  if (!srcFilePath || !destFilePath) {
    return;
  }
  fs.readFile(srcFilePath, 'utf8', readingFile);
  function readingFile(error, data) {
      if (error) {
        console.log(error);
        return;
      }
      fs.writeFile(destFilePath, data, 'utf8', writeFile);
  }
  function writeFile(error) {
      if (error) {
          console.log(error);
      }
  }
};

/*
 * Main loop - Process all images in the folder
 * Optimize filename, resolution and file-size
 */
fs.readdir(directoryPath, async (err, files) => {
    if (err) {
        return console.log('Unable to scan directory: ' + err);
    }
    files.forEach(async file => {

        if (file.match(/.*\.(jpg|jpeg|png|gif)$/i)) {
          // This is an image => detect dimensions
          let imageSize = false;
          try {
            imageSize = await getImageSize(file);
            console.log('#1a Processing image: ', file, imageSize);
          } catch (error) {
            console.error('#1a Processing image error ', error);
          }

          // Optimize file name => lowercase + add prefix + fix sort (pad numbers) + ext
          let finalFile =
              addPrefix(params.prefix) +
              prettify(params.padNumbers ? padNumbers(removeFileExt(file)) : removeFileExt(file)) +
              getFileExt(file)
                  .replace(/\.(jpg|jpeg|png|gif)$/i, `.${params.ext}`)

          // Optimize resolution => Upscale 2x if required
          if (params.upscaleDeepAiKey && imageSize.height < params.height) {
              console.debug('#2 This image needs upscaling:', file, imageSize);
              file = await upscaleImage(file);
          }

          // Optimize file size => Convert to jpg, with selected height and quality
          normalizeImageFile(file, finalFile, params.height, params.quality);
        }

        if (file.match(/.*\.svg$/)) {
          console.log('#1b Processing image: ', file);
          let finalFile =
              addPrefix(params.prefix) +
              prettify(params.padNumbers ? padNumbers(removeFileExt(file)) : removeFileExt(file)) +
              getFileExt(file);
          copyFile(file, finalFile);
          console.log('READY => ', finalFile);
        }
    });
});

