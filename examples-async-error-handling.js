// TEST Async behavior: Callbacks vs async/await Promise
// Autorun
//

// const failed = false;
const failed = true;
const errorData = { message: 'failed' };
const successData = { status: 'success' };
const delayDuration = 500;

/*
 * 1. Callback version (oldschool not recommended)
 */
function delayWithCallback(callback) {
    setTimeout(() => {
        if (failed) {
            callback(errorData, false);
        } else {
            callback(false, successData);
        }
    }, delayDuration);
}

// Initialize
// delayWithCallback((err, data) => {
//     if (err) {
//         console.error('delayWithCallback outside error: ' + JSON.stringify(err));
//     }
//     console.debug('delayWithCallback data', data);
// });


/*
 * 2. Async await withoud catch inside: (dangerous) requires outside promise.catch()
 */
function delayWithPromise() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (failed) {
                reject(errorData);
            } else {
                resolve(successData);
            }
        }, delayDuration);
    });
}

// Initialize
// (async () => {
//     const data = await delayWithPromise().catch(err =>
//         console.error('delayWithPromise outside catch exception: ' + JSON.stringify(err))
//     );
//     console.debug('delayWithPromise data', data);
// })();


/*
 * 3. Async await destructured (recommended)
 */
function delayWithPromiseDestructured() {
    return new Promise((resolve/*, reject*/) => {
        setTimeout(() => {
            // if (failed.some.undef.prop) { // ! this triggers: unhandled exception: Cannot read properties of undefined
            if (failed) {
                // reject(errorData); // will search for the nearest .catch() handler
                resolve([errorData, false]);
            } else {
                resolve([false, successData]);
            }
            resolve();
        }, delayDuration);
    }).catch(err => [{ ...errorData, messageDetailed: err }, false]);
}

// Initialize
(async () => {
    try {
        const [err, res] = await delayWithPromiseDestructured();
        console.debug('delayWithPromiseDestructured err', err);
        console.debug('delayWithPromiseDestructured res', res);
    } catch (err) {
        console.error('delayWithPromiseDestructured outside catch err', err);
    }
})();

/*
 * 4. Async await destructured with extra try catch inside
 */
// function delayWithPromiseDestructuredWithTryCatchInside() {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             try {
//                 if (failed.some.undef.value) {
//                 // if (failed) {
//                     reject(errorData);
//                 } else {
//                     resolve(successData);
//                 }
//             } catch (err) {
//                 console.error('delayWithPromiseDestructuredWithTryCatchInside inside catch error', err);
//                 reject({ ...errorData, messageDetailed: err });
//             }
//         }, delayDuration);
//     }).catch(err => {
//         return [err, false];
//     });
// }

// // Initialize
// (async () => {
//     try {
//         const [err, res] = await delayWithPromiseDestructuredWithTryCatchInside();
//         console.debug('delayWithPromiseDestructuredWithTryCatchInside err, res', err, res);
//     } catch (err) {
//         console.error('delayWithPromiseDestructuredWithTryCatchInside outside catch error', err);
//     }
// })();


// try{
//     // undefinedfunction()
//     if (some.undefined.value) {

//     }
//     console.log('I guess you do exist')
// }
// catch(e){
//     console.log('An error has occurred: '+e.message)
// }
// finally{
//     console.log('I am alerted regardless of the outcome above')
// }



