'use strict';
const fs = require('fs');
require('log-timestamp');
const { exec } = require('child_process');

console.log(`Watching for file changes ...`);

const files = [
'./views/application.js',
'./views/template1.xml',
'./views/template2.xml',
'./views/template3.xml',
'./views/template4.xml',
'./views/template5.xml',
'./views/template6.xml'
];

function watchFile(file) {
    fs.watchFile(file, (curr, prev) => {
        console.log(`${file} changed! Minifying ...`);
        exec(`yarn minify`, (err, stdout, stderr) => {
            if (err) {
                console.error(err);
            } else {
                // if (stdout) console.log(`stdout: ${stdout}`);
                if (stderr) console.log(`stderr: ${stderr}`);
            }
        });
    });
}

files.forEach(f => watchFile(f));

