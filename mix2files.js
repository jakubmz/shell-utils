/*
 * Mix 2 files line by line
 * used for mixing keys with values
 *
 * Output is like:
 *  key value
 *  file1-line1 file2-line1
 *  file1-line2 file2-line2
 *  ...
 *  file1-lineN file2-lineN
 *
 * Author: Jakub Michal Zawada Zegan (jakubmz@gmail.com)
 */

const fs = require('fs');

const file1 = process.argv[2];
const file2 = process.argv[3];
let file1Array, file2Array;

if (!file1 || !file2) {
    console.error('Use: node mix2Files.js file1 file2');
    return;
}

fs.readFile(file1, 'utf8', (err1, data1) => {
    if (err1) {
        return console.error(err1);
    }
    file1Array = data1.split('\n');
    fs.readFile(file2, 'utf8', (err2, data2) => {
        if (err2) {
            return console.error(err2);
        }
        file2Array = data2.split('\n');
        const maxLinesCount = Math.max(file1Array.length, file2Array.length);
        for (let l = 0; l < maxLinesCount; l++) {
            console.log(`${file1Array[l] || ''} ${file2Array[l] || ''}`);
        }
    });
});