/*
 * Take screenshot from url
 *
 * Created by jakubmz on 21 March 2022
 */

const puppeteer = require('puppeteer'); // Full version: ships with Chromium
// const puppeteer = require('puppeteer-core'); // Compact version: must use an installed Chrome or Firefox

const url = process.argv[2] || 'https://public.doorcount.com/?client=Eq6CQfCMJ1e1srR4YUsm&layout=Stacked&location=ICYyJfenECTOF1MvUjKe&views%5B%5D=Forecast&customBranding%5B%5D=0';

(async () => {

    const browser = await puppeteer.launch({
        headless: true//,
        // executablePath: '/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome' // use installed Chrome instead of Chromium
    });
    const page = await browser.newPage();

    await page.setViewport({
        width: 1500,
        height: 1000,
        deviceScaleFactor: 1,
    });

    // await page.goto(url, { waitUntil: 'networkidle2' });
    await page.goto(url);

    // Wait 10 seconds and make screenshot
    await setTimeout(async () => {

        console.debug('### taking a screenshot ...');
        const data = await page.screenshot({ path: 'screenshot.png' });
        console.debug('### result screenshot data: ', data);

        await browser.close();

        // Return raw data, imageUrl, or save to amazon S3
       return data;

    }, 10000);

    // Wait till especified element is loaded - doesnt work well if show/hide is used
    // const elem = await page.$('.recharts-surface');

    // PDF support
    // await page.pdf({ path: 'page.pdf' }); // Some visible render issues

})();

// TODO is there some lambda with dektop ubuntu ?
