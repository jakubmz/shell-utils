/*
 * Converts 2 column tsv file (keyword\ttranslation) into JSON output
 */
'esversion: 6';

const fs = require('fs');

// Addtional settings
const sorted = true;
const uniques = true;
const skipEmpty = true;

const scriptName = process.argv[1].replace(/^.*\//, '');
const file = process.argv[2];

if (!file) {
    console.error(`Use: node ${scriptName} file.tsv`);
    return;
}

const onlyUnique = (value, index, self) => self.indexOf(value) === index;

fs.readFile(file, 'utf8', (err, data) => {
    'use strict';
    if (err) {
        return console.error(err);
    }
    let lines = data.split('\n');
    if (sorted) {
        lines.sort();
    }
    if (uniques) {
        lines = lines.filter(onlyUnique);
    }

    console.log('{');
    lines.forEach((l,i) => {
        let [col1, col2] = l.replace('\r', '').split('\t');
        if (!skipEmpty || (skipEmpty && col1 && col2)) {
            console.log(`  "${col1}": "${col2}"` + (lines.length > i + 1 ? ',' : ''));
        }
    });
    console.log('}');
});