/*
 * Watermark an image
 *
 * ACTIONS:
 * - add watermark to an image and save
 *
 * USE:
 * 1. Copy this script inside folder with images (preferably inside master - to resolve Jimp)
 * 2: Run: node watermarklImage.js image.jpg watermark.jpg
 */
'esversion: 6';

// const fs = require('fs');
var watermark = require('jimp-watermark');

const image = String(process.argv[2]);
const watermarkImage = String(process.argv[3]);

console.log(process.argv);

if (!image || !watermarkImage) {
    console.error('Use: node watermarklImage.js image.jpg watermark.jpg');
}

console.log(`\n\nwatermark-image.js running with params => image=${image}, watermarkImage=${watermarkImage} ...`);

const options = {
	'ratio': 0.4,// Should be less than one
    'opacity': 0.3, //Should be less than one
    'dstPath' : 'watermarked-' + image
};

watermark.addWatermark(image, watermarkImage, options);

// watermark.addTextWatermark('/path/to/image/file', options);
