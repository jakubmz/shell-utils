/*
 * Extract all occurrences of regex from file onto a list
 *
 * Output is like:
 *  key value
 *  key1
 *  key2
 *  key3
 *  ...
 *  keyN
 *
 * Author: Jakub Michal Zawada Zegan (jakubmz@gmail.com)
 */

const fs = require('fs');

const file = process.argv[3];
const keywordRegex = process.argv[2];

if (!keywordRegex || !file) {
    console.error('Use: node extractStringsFromFile.js keywordRegex file');
    return;
}

fs.readFile(file, 'utf8', (err, data) => {
    'use strict';
    if (err) {
        return console.error(err);
    }
    // console.log(data);
    // const regexp = new RegExp(keywordRegex, 'g');
    // const list = data.match(regexp);
    const list = data.match(/\{\{.* \| translate\}\}/g);
    console.log(list);
});