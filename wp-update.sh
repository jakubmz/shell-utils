#!/bin/sh
# Wordpress update script - loads remote project files and db

USER=kuba
HOST=mufrons2
REMOTE_PATH=/home/kuba/projects/masquetiza

DB=yes
FILES=yes

if [ "$1" == "db" ] ; then
    echo -ne "Local database"
    CODE=no
    FILES=no
elif [ "$1" == "files" ] ; then
    echo -ne "Local files"
    CODE=no
    DB=no
else
    echo -ne "Local database and files"
fi

read -p " will be overriden! This action can not be undone. Are you sure? " -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    exit 1
fi


echo "--- RUNNING UPDATE ..."
date

if [ "$DB" == "yes" ] ; then
    echo
    echo "--- REMOTE DB EXPORT"
    DUMP_FILE=masquetiza-db.sql
    ssh ${USER}@${HOST} "docker exec masquetiza_db_1 bash -c \"mysqldump -u wordpress wordpress -pwordpress |grep -v 'mysqldump: .Warning'\"" > ./db-dump/${DUMP_FILE}
    docker exec masquetiza_db_1 bash -c "mysql -uwordpress -pwordpress wordpress < /tmp/db-dump/${DUMP_FILE}"
fi

if [ "$FILES" == "yes" ] ; then
    echo
    echo "--- REMOTE FILE SYNC ..."
    rsync -zarvh --no-perms --include ".*" --exclude 'backup-guard/*' $USER@$HOST:$REMOTE_PATH/uploads/* uploads --delete
    rsync -zarvh --no-perms --include ".*" $USER@$HOST:$REMOTE_PATH/plugins/* plugins --delete
fi

echo
date
echo "--- UPDATE COMPLETE!"
