#!bin/bash
# Minify JS and XML files

cat views/template1.xml |sed -e :a -e '$!N;s/\n//;ta' |sed -e :a -e '$!N;s/\t//;ta' > views/template1.xml.ejs
cat views/template2.xml |sed -e :a -e '$!N;s/\n//;ta' |sed -e :a -e '$!N;s/\t//;ta' > views/template2.xml.ejs
cat views/template3.xml |sed -e :a -e '$!N;s/\n//;ta' |sed -e :a -e '$!N;s/\t//;ta' > views/template3.xml.ejs
cat views/template4.xml |sed -e :a -e '$!N;s/\n//;ta' |sed -e :a -e '$!N;s/\t//;ta' > views/template4.xml.ejs
cat views/template5.xml |sed -e :a -e '$!N;s/\n//;ta' |sed -e :a -e '$!N;s/\t//;ta' > views/template5.xml.ejs
cat views/template6.xml |sed -e :a -e '$!N;s/\n//;ta' |sed -e :a -e '$!N;s/\t//;ta' > views/template6.xml.ejs

yarn terser --compress -f quote_style=3 views/application.js -o views/application.min.js